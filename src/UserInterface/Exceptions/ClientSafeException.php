<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Exceptions;

class ClientSafeException extends \Exception
{
    public static function fromException(\Exception $exception, int $code): self
    {
        return new self($exception->getMessage(), $code, $exception);
    }
}

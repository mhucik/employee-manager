<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Cli;

use Mhucik\Infrastructure\XmlDatabase\Config;
use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tests\IntegrationTests\Fixtures\EmployeeFixture;

class InitDbCommand extends Command
{
    public function __construct(
        private XmlManager $xmlManager,
        private Config $config,
    )
    {
        parent::__construct('app:initDb');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        file_put_contents($this->config->pathToXml, "<?xml version=\"1.0\"?>\n<listEmployees></listEmployees>");

        $employeeFixture = new EmployeeFixture($this->config->pathToXml);

        $employeeFixture->load($this->xmlManager);

        return 0;
    }
}

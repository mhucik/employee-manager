<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQuery;
use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQueryHandlerInterface;
use Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException;
use Mhucik\EmployeeManager\UserInterface\Exceptions\ClientSafeException;
use Nette\Utils\Json;

/**
 * @Path("/employees")
 */
class EmployeeDetailController extends BaseV1Controller
{
    public function __construct(
        private EmployeeDetailQueryHandlerInterface $queryHandler,
    ) {}

    /**
     * @Path("/{employeeId}")
     * @Method("GET")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        /** @var string $employeeId */
        $employeeId = $request->getParameter('employeeId');

        $query = new EmployeeDetailQuery($employeeId);

        try {
            $result = $this->queryHandler->handle($query);
        } catch (EmployeeNotFoundException $e) {
            throw ClientSafeException::fromException($e, ApiResponse::S404_NOT_FOUND);
        }

        $response = $response
            ->writeBody(Json::encode(['data' => $result->toArray()]))
        ;

        return $response;
    }
}

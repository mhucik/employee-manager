<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQuery;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQueryHandlerInterface;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQueryResult;
use Nette\Utils\Json;

/**
 * @Path("/employees")
 */
class AgeStatsDataController extends BaseV1Controller
{
    public function __construct(
        private AgeStatsDataQueryHandlerInterface $ageStatsDataQueryHandler
    ) {}


    /**
     * @Path("/age-stats/{binSize}")
     * @Method("GET")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        $result = $this->ageStatsDataQueryHandler->handle(new AgeStatsDataQuery(intval($request->getParameter('binSize'))));

        $response = $response
            ->writeBody(Json::encode($result->toArray()))
        ;

        return $response;
    }
}

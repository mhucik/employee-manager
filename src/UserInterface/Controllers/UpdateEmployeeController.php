<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Commands\UpdateEmployee\UpdateEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\UpdateEmployee\UpdateEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Mhucik\EmployeeManager\UserInterface\Exceptions\ClientSafeException;

/**
 * @Path("/employees")
 */
class UpdateEmployeeController extends BaseV1Controller
{
    public function __construct(
        private UpdateEmployeeCommandHandler $updateEmployeeCommandHandler
    ) {}


    /**
     * @Path("/{id}")
     * @Method("PATCH")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        /** @var string $id */
        $id = $request->getParameter('id');
        /** @var array<string> $body */
        $body = $request->getJsonBody();

        try {
            $this->updateEmployeeCommandHandler->handle(
                new UpdateEmployeeCommand(
                    $id,
                    $body['name'] ?? null,
                    isset($body['age']) ? intval($body['age']) : null,
                    $body['sex'] ?? null,
                )
            );
        } catch (InvalidArgumentException $e) {
            throw ClientSafeException::fromException($e, ApiResponse::S412_PRECONDITION_FAILED);
        }

        $response = $response
            ->withStatus(ApiResponse::S204_NO_CONTENT);

        return $response;
    }
}

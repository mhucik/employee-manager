<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Commands\CreateEmployee\CreateEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\CreateEmployee\CreateEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Mhucik\EmployeeManager\UserInterface\Exceptions\ClientSafeException;
use Nette\Utils\Json;

/**
 * @Path("/employees")
 */
class CreateEmployeeController extends BaseV1Controller
{
    public function __construct(
        private CreateEmployeeCommandHandler $createEmployeeCommandHandler
    ) {}


    /**
     * @Path("/")
     * @Method("POST")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        /** @var array<string> $body */
        $body = $request->getJsonBody();

        try {
            $result = $this->createEmployeeCommandHandler->handle(
                new CreateEmployeeCommand(
                    $body['name'],
                    intval($body['age']),
                    $body['sex'],
                )
            );
        } catch (InvalidArgumentException $e) {
            throw ClientSafeException::fromException($e, ApiResponse::S412_PRECONDITION_FAILED);
        }

        $response = $response
            ->writeBody(Json::encode(['id' => $result]))
        ;

        return $response;
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQuery;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQueryHandlerInterface;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQueryResult;
use Nette\Utils\Json;

/**
 * @Path("/employees")
 */
class EmployeesListController extends BaseV1Controller
{
    public function __construct(
        private EmployeesListQueryHandlerInterface $employeesListQueryHandler
    ) {}


    /**
     * @Path("/")
     * @Method("GET")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        $result = $this->employeesListQueryHandler->handle(new EmployeesListQuery());

        $response = $response
            ->writeBody(Json::encode($result->toArray()))
        ;

        return $response;
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Controllers;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\Application\Commands\DeleteEmployee\DeleteEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\DeleteEmployee\DeleteEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException;
use Mhucik\EmployeeManager\UserInterface\Exceptions\ClientSafeException;

/**
 * @Path("/employees")
 */
class DeleteEmployeeController extends BaseV1Controller
{
    public function __construct(
        private DeleteEmployeeCommandHandler $deleteEmployeeCommandHandler
    ) {}


    /**
     * @Path("/{id}")
     * @Method("DELETE")
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        /** @var string $id */
        $id = $request->getParameter('id');

        try {
            $this->deleteEmployeeCommandHandler->handle(new DeleteEmployeeCommand($id));
        } catch (EmployeeNotFoundException $e) {
            throw ClientSafeException::fromException($e, ApiResponse::S404_NOT_FOUND);
        }

        $response = $response
            ->withStatus(ApiResponse::S204_NO_CONTENT);

        return $response;
    }
}

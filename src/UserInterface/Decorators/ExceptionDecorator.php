<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Decorators;

use Apitte\Core\Decorator\IErrorDecorator;
use Apitte\Core\Exception\ApiException;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Mhucik\EmployeeManager\UserInterface\Exceptions\ClientSafeException;
use Nette\Utils\Json;

class ExceptionDecorator implements IErrorDecorator
{
    public function decorateError(ApiRequest $request, ApiResponse $response, ApiException $error): ApiResponse
    {
        $previous = $error->getPrevious();
        $errorMessage = $previous instanceof ClientSafeException ? $previous->getMessage() : $error->getMessage();
        $status = $previous instanceof ClientSafeException ? $previous->getCode() : $error->getCode();

        $response = $response
            ->withStatus($status)
            ->withHeader('Content-Type', 'application/json')
            ->writeBody(Json::encode([
                'error' => $errorMessage,
            ]));

        return $response;
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\UserInterface\Decorators;

use Apitte\Core\Decorator\IResponseDecorator;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;

class JsonDecorator implements IResponseDecorator
{
    public function decorateResponse(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        $response = $response->withHeader('Content-Type', 'application/json');

        return $response;
    }
}

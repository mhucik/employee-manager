<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\CreateEmployee;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Entities\Employee;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mhucik\EmployeeManager\Domain\ValueObjects\Age;
use Mhucik\EmployeeManager\Domain\ValueObjects\Name;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;

class CreateEmployeeCommandHandler
{
    public function __construct(
        private EmployeeRepositoryInterface $employeeRepository,
    ) {}


    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException
     */
    public function handle(CreateEmployeeCommand $command): string
    {
        $employee = new Employee(
            EmployeeId::generate(),
            Name::fromString($command->name),
            Age::fromValue($command->age),
            Sex::from($command->sex),
        );

        $this->employeeRepository->save($employee);

        return $employee->getEmployeeId()->toString();
    }
}

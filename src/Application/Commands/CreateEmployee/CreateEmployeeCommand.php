<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\CreateEmployee;

readonly class CreateEmployeeCommand
{
    public function __construct(
        public string $name,
        public int $age,
        public string $sex,
    ) {}
}

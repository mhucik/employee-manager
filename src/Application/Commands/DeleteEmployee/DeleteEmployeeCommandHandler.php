<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\DeleteEmployee;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;

class DeleteEmployeeCommandHandler
{
    public function __construct(
        private EmployeeRepositoryInterface $employeeRepository,
    ) {}


    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException
     */
    public function handle(DeleteEmployeeCommand $command): void
    {
        $employee = $this->employeeRepository->getById(EmployeeId::fromString($command->id));

        $employee->delete();

        $this->employeeRepository->save($employee);
    }
}

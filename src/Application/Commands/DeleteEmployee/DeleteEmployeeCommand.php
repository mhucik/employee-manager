<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\DeleteEmployee;

readonly class DeleteEmployeeCommand
{
    public function __construct(
        public string $id,
    ) {}
}

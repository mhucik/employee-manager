<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\UpdateEmployee;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mhucik\EmployeeManager\Domain\ValueObjects\Age;
use Mhucik\EmployeeManager\Domain\ValueObjects\Name;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;

class UpdateEmployeeCommandHandler
{
    public function __construct(
        private EmployeeRepositoryInterface $employeeRepository,
    ) {}


    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException
     */
    public function handle(UpdateEmployeeCommand $updateEmployeeCommand): void
    {
        $employee = $this->employeeRepository->getById(EmployeeId::fromString($updateEmployeeCommand->id));

        $employee->update(
            $updateEmployeeCommand->age ? Age::fromValue($updateEmployeeCommand->age) : null,
            $updateEmployeeCommand->name ? Name::fromString($updateEmployeeCommand->name) : null,
            $updateEmployeeCommand->sex ? Sex::from($updateEmployeeCommand->sex) : null,
        );

        $this->employeeRepository->save($employee);
    }
}

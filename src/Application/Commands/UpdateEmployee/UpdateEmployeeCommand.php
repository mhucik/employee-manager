<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Commands\UpdateEmployee;

readonly class UpdateEmployeeCommand
{
    public function __construct(
        public string $id,
        public ?string $name,
        public ?int $age,
        public ?string $sex,
    ) {}
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\EmployeesList;

readonly class EmployeesListQueryResult
{
    /**
     * @param array<int, \Mhucik\EmployeeManager\Application\Queries\EmployeesList\Employee> $employees
     */
    public function __construct(
        public int $total,
        public array $employees,
    ) {}


    /**
     * @return array<string, array<int, array<string, string|int>>|int>
     */
    public function toArray(): array
    {
        return [
            'total' => $this->total,
            'employees' => array_map(fn(Employee $employee): array => $employee->toArray(), $this->employees),
        ];
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\EmployeeDetail;

readonly class EmployeeDetailQuery
{
    public function __construct(
        public string $employeeId,
    ) {}
}

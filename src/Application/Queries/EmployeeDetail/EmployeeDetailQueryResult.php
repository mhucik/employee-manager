<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\EmployeeDetail;

readonly class EmployeeDetailQueryResult
{
    public function __construct(
        public string $id,
        public string $name,
        public int $age,
        public string $sex,
    ) {}


    /**
     * @return array<string, string|int>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'age' => $this->age,
            'sex' => $this->sex,
        ];
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\EmployeeDetail;

interface EmployeeDetailQueryHandlerInterface
{
    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException
     */
    public function handle(EmployeeDetailQuery $query): EmployeeDetailQueryResult;
}

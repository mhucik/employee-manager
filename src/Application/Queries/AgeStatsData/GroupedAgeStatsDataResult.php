<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\AgeStatsData;

class GroupedAgeStatsDataResult
{
    public function __construct(
        public string $ageInterval,
        public int $frequency,
    ) {}


    /**
     * @return array<string, string|int>
     */
    public function toArray(): array
    {
        return [
            'ageInterval' => $this->ageInterval,
            'frequency' => $this->frequency,
        ];
    }
}

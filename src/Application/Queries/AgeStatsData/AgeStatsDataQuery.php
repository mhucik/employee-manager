<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\AgeStatsData;

readonly class AgeStatsDataQuery
{
    public function __construct(
        public int $binSize,
    ) {}
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\AgeStatsData;

readonly class AgeStatsDataQueryResult
{
    /**
     * @param GroupedAgeStatsDataResult[] $maleStats
     * @param GroupedAgeStatsDataResult[] $femaleStats
     * @param GroupedAgeStatsDataResult[] $totalStats
     */
    public function __construct(
        public array $maleStats,
        public array $femaleStats,
        public array $totalStats,
    ) {}


    /**
     * @return array<string, array<array<string, string|int>>>
     */
    public function toArray(): array
    {
        return [
            'maleStats' => array_map(fn(GroupedAgeStatsDataResult $result): array => $result->toArray(), $this->maleStats),
            'femaleStats' => array_map(fn(GroupedAgeStatsDataResult $result): array => $result->toArray(), $this->femaleStats),
            'totalStats' => array_map(fn(GroupedAgeStatsDataResult $result): array => $result->toArray(), $this->totalStats),
        ];
    }
}

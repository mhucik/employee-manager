<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Application\Queries\AgeStatsData;

interface AgeStatsDataQueryHandlerInterface
{
    public function handle(AgeStatsDataQuery $query): AgeStatsDataQueryResult;
}

<?php declare(strict_types = 1);

use Nette\Bootstrap\Configurator;

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Configurator();

$debugMode = (bool) getenv('IS_LOCAL');
$configurator->setDebugMode($debugMode);
$configurator->addConfig(__DIR__ . '/UserInterface/config/config.neon');
$configurator->setTempDirectory(__DIR__ . '/../temp/');
$configurator->enableTracy(__DIR__ . '/../log');


return $configurator;

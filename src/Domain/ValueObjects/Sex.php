<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\ValueObjects;

enum Sex: string
{
    case MALE = 'male';
    case FEMALE = 'female';
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Stringable;

class Name implements Stringable
{
    const NAME_REGEX = '/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ∂ð ,.\'-]+$/u';


    private function __construct(
        private string $name,
    )
    {
        if (empty($name)) {
            throw new InvalidArgumentException('Name cannot be empty');
        }

        if (preg_match(self::NAME_REGEX, $name) === 0) {
            throw new InvalidArgumentException('Name contains invalid characters');
        }
    }


    public static function fromString(string $name): self
    {
        return new self($name);
    }


    public function toString(): string
    {
        return $this->name;
    }


    public function __toString(): string
    {
        return $this->toString();
    }
}

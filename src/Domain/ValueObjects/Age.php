<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Stringable;

class Age implements Stringable
{
    public const AGE_LIMIT = 18;

    private int $age;


    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException
     */
    private function __construct(
        mixed $value,
    )
    {
        if ( ! is_numeric($value)) {
            throw new InvalidArgumentException('Age must be integer');
        }

        $age = intval($value);

        if ($age != $value) {
            throw new InvalidArgumentException('Age must be integer');
        }

        if ($age < self::AGE_LIMIT) {
            throw new InvalidArgumentException(sprintf('Age must be greater or equal to %d', self::AGE_LIMIT));
        }

        $this->age = $age;
    }


    /**
     * @throws InvalidArgumentException
     */
    public static function fromValue(mixed $age): self
    {
        return new self($age);
    }


    public function toInt(): int
    {
        return $this->age;
    }


    public function __toString(): string
    {
        return (string) $this->age;
    }
}

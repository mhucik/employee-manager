<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\Repositories;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Entities\Employee;

interface EmployeeRepositoryInterface
{
    /**
     * @throws \Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException
     */
    public function getById(EmployeeId $id): Employee;


    public function save(Employee $employee): void;
}

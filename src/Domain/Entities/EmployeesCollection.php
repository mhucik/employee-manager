<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\Entities;

use KudrMichal\Serializer\Xml\Metadata as XML;
use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;

#[XML\Document(name: "listEmployees")]
class EmployeesCollection implements \IteratorAggregate, \Countable
{
    /**
     * @var array<int, \Mhucik\EmployeeManager\Domain\Entities\Employee>
     */
    #[XML\ElementArray(name: 'employees', itemName: 'employee', type: Employee::class)]
    private array $employees = [];

    public function __construct(
        Employee... $employees
    )
    {
        $this->employees = $employees; // @phpstan-ignore-line
    }


    public function add(Employee $employee): void
    {
        $existingEmployeeIndex = $this->findEmployeeIndex($employee->getEmployeeId());

        if ($existingEmployeeIndex !== null) {
            $this->employees[$existingEmployeeIndex] = $employee;

            return;
        }

        $this->employees[] = $employee;
    }

    /**
     * @return \ArrayIterator<int, \Mhucik\EmployeeManager\Domain\Entities\Employee>
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->employees);
    }

    public function count(): int
    {
        return count($this->employees);
    }


    public function findEmployee(EmployeeId $employeeId): ?Employee
    {
        foreach ($this->employees as $employee) {
            if ($employee->getEmployeeId()->equals($employeeId)) {
                return $employee;
            }
        }

        return null;
    }


    public function findEmployeeIndex(EmployeeId $employeeId): ?int
    {
        foreach ($this->employees as $index => $employee) {
            if ($employee->getEmployeeId()->equals($employeeId)) {
                return $index;
            }
        }

        return null;
    }


    public function getNotDeletedEmployees(): EmployeesCollection
    {
        $notDeletedEmployees = [];

        foreach ($this->employees as $employee) {
            if ( ! $employee->isDeleted()) {
                $notDeletedEmployees[] = $employee;
            }
        }

        return new EmployeesCollection(...$notDeletedEmployees);
    }


    public function getEmployeesBySex(Sex $sex): EmployeesCollection
    {
        $manEmployees = [];

        foreach ($this->employees as $employee) {
            if ($employee->getSex() === $sex) {
                $manEmployees[] = $employee;
            }
        }

        return new EmployeesCollection(...$manEmployees);
    }
}

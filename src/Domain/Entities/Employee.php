<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\Entities;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\ValueObjects\Age;
use Mhucik\EmployeeManager\Domain\ValueObjects\Name;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use KudrMichal\Serializer\Xml\Metadata as XML;
use Mhucik\Infrastructure\XmlDatabase\Serializers\AgeParser;
use Mhucik\Infrastructure\XmlDatabase\Serializers\EmployeeIdParser;
use Mhucik\Infrastructure\XmlDatabase\Serializers\NameParser;
use Mhucik\Infrastructure\XmlDatabase\Serializers\SexParser;

class Employee
{
    private bool $deleted = false;

    public function __construct(
        #[XML\Element(name: "employeeId", callable: [EmployeeIdParser::class, 'parse'])]
        private EmployeeId $employeeId,
        #[XML\Element(name: "name", callable: [NameParser::class, 'parse'])] private Name $name,
        #[XML\Element(name: "age", callable: [AgeParser::class, 'parse'])] private Age $age,
        #[XML\Element(name: "sex", callable: [SexParser::class, 'parse'])] private Sex $sex,
    ) {}


    public function update(
        ?Age $age = null,
        ?Name $name = null,
        ?Sex $sex = null,
    ): self
    {
        if ($age !== null) {
            $this->age = $age;
        }

        if ($name !== null) {
            $this->name = $name;
        }

        if ($sex !== null) {
            $this->sex = $sex;
        }

        return $this;
    }


    public function delete(): self
    {
        $this->deleted = true;

        return $this;
    }


    public function isDeleted(): bool
    {
        return $this->deleted;
    }


    public function getEmployeeId(): EmployeeId
    {
        return $this->employeeId;
    }


    public function getName(): Name
    {
        return $this->name;
    }


    public function getAge(): Age
    {
        return $this->age;
    }


    public function getSex(): Sex
    {
        return $this->sex;
    }
}

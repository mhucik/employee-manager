<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\Exceptions;

use Exception;

class InvalidArgumentException extends Exception
{
}

<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\Exceptions;

use Exception;
use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;

class EmployeeNotFoundException extends Exception
{
    public static function withId(EmployeeId $employeeId): self
    {
        return new self(sprintf('Employee with id %s not found.', $employeeId->toString()));
    }
}

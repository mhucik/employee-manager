<?php declare(strict_types = 1);

namespace Mhucik\EmployeeManager\Domain\AggregateId;

use Ramsey\Uuid\UuidInterface;
use Stringable;

class EmployeeId implements Stringable
{
    private UuidInterface $uuid;

    private function __construct(
        string $id,
    )
    {
        $this->uuid = \Ramsey\Uuid\Uuid::fromString($id);
    }


    public static function generate(): self
    {
        return new self(\Ramsey\Uuid\Uuid::uuid4()->toString());
    }


    public static function fromString(string $id): self
    {
        return new self($id);
    }


    public function toString(): string
    {
        return $this->uuid->toString();
    }


    public function __toString(): string
    {
        return $this->toString();
    }


    public function equals(self $other): bool
    {
        return $this->uuid->equals($other->uuid);
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\Repositories;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Entities\Employee;
use Mhucik\EmployeeManager\Domain\Entities\EmployeesCollection;
use Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mhucik\Infrastructure\XmlDatabase\Config;
use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;

class XmlEmployeeRepository implements EmployeeRepositoryInterface
{
    public function __construct(
        private XmlManager $xmlManager,
        private Config $config,
    ) {}


    public function getById(EmployeeId $id): Employee
    {
        $employeesCollection = $this->xmlManager->loadData($this->config->xmlDocument, EmployeesCollection::class);

        $employee = $employeesCollection->findEmployee($id);

        if ( ! $employee) {
            throw EmployeeNotFoundException::withId($id);
        }

        return $employee;
    }


    public function save(Employee $employee): void
    {
        $employeesCollection = $this->xmlManager->loadData($this->config->xmlDocument, EmployeesCollection::class);

        $employeesCollection->add($employee);

        $this->xmlManager->saveData($this->config->pathToXml, $employeesCollection->getNotDeletedEmployees());
    }
}

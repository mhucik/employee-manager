<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\Queries\AgeStatsData;

use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\GroupedAgeStatsDataResult;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQuery;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQueryHandlerInterface;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQueryResult;
use Mhucik\EmployeeManager\Domain\Entities\Employee;
use Mhucik\EmployeeManager\Domain\Entities\EmployeesCollection;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use Mhucik\Infrastructure\XmlDatabase\Config;
use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;

class XmlAgeStatsDataQueryHandler implements AgeStatsDataQueryHandlerInterface
{
    public function __construct(
        private XmlManager $xmlManager,
        private Config $config,
    ) {}


    public function handle(AgeStatsDataQuery $query): AgeStatsDataQueryResult
    {
        $maleAges = array_map(
            fn(Employee $employee) => $employee->getAge()->toInt(),
            iterator_to_array($this->loadEmployeesCollection()->getEmployeesBySex(Sex::MALE))
        );
        $femaleAges = array_map(
            fn(Employee $employee) => $employee->getAge()->toInt(),
            iterator_to_array($this->loadEmployeesCollection()->getEmployeesBySex(Sex::FEMALE))
        );

        $minAge = min(array_merge($maleAges, $femaleAges));
        $maxAge = max(array_merge($maleAges, $femaleAges));

        $noOfBins = $query->binSize;
        $binSize = intval(ceil(($maxAge - $minAge) / $noOfBins));
        $maleBinFrequencies = $this->getFrequencies($maleAges, $minAge, $binSize, $noOfBins);
        $femaleBinFrequencies = $this->getFrequencies($femaleAges, $minAge, $binSize, $noOfBins);
        $totalBinFrequencies = $this->getFrequencies(array_merge($maleAges, $femaleAges), $minAge, $binSize, $noOfBins);

        return new AgeStatsDataQueryResult(
            $this->mapToAgeStatsToDataResult($maleBinFrequencies, $minAge, $binSize, $maxAge),
            $this->mapToAgeStatsToDataResult($femaleBinFrequencies, $minAge, $binSize, $maxAge),
            $this->mapToAgeStatsToDataResult($totalBinFrequencies, $minAge, $binSize, $maxAge)
        );
    }


    /**
     * @param array<int, int> $binFrequencies
     * @return array<int, GroupedAgeStatsDataResult>
     */
    private function mapToAgeStatsToDataResult(array $binFrequencies, int $minAge, int $binSize, int $maxAge): array
    {
        $result = [];

        foreach ($binFrequencies as $index => $frequency) {
            $from = $minAge + $index * $binSize;
            $to = $index === (count($binFrequencies) - 1) ? $maxAge : $minAge + ($index + 1) * $binSize - 1;

            $result[] = new GroupedAgeStatsDataResult(
                sprintf('%d - %d', $from, $to),
                $frequency,
            );
        }

        return $result;
    }


    /**
     * @param array<int, int> $ages
     * @return array<int, int>
     */
    private function getFrequencies(array $ages, int $minAge, int $binSize, int $noOfBins): array
    {
        $binFrequencies = array_fill(0, $noOfBins, 0);

        foreach ($ages as $age) {
            $intervalIndex = min(intval(($age - $minAge) / $binSize), $noOfBins - 1);
            $binFrequencies[$intervalIndex]++;
        }

        return $binFrequencies;
    }


    private function loadEmployeesCollection(): EmployeesCollection
    {
        return $this->xmlManager->loadData($this->config->xmlDocument, EmployeesCollection::class);
    }
}

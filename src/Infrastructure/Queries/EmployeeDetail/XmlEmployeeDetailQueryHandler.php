<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\Queries\EmployeeDetail;

use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQuery;
use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQueryHandlerInterface;
use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQueryResult;
use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;

class XmlEmployeeDetailQueryHandler implements EmployeeDetailQueryHandlerInterface
{
    public function __construct(
        private EmployeeRepositoryInterface $xmlEmployeeRepository,
    ) {}


    public function handle(EmployeeDetailQuery $query): EmployeeDetailQueryResult
    {
        $employeeId = EmployeeId::fromString($query->employeeId);
        $employee = $this->xmlEmployeeRepository->getById($employeeId);

        return new EmployeeDetailQueryResult(
            $employee->getEmployeeId()->toString(),
            $employee->getName()->toString(),
            $employee->getAge()->toInt(),
            $employee->getSex()->value,
        );
    }
}

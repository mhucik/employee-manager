<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\Queries\EmployeesList;

use Mhucik\EmployeeManager\Application\Queries\EmployeesList\Employee;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQuery;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQueryHandlerInterface;
use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQueryResult;
use Mhucik\EmployeeManager\Domain\Entities\EmployeesCollection;
use Mhucik\Infrastructure\XmlDatabase\Config;
use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;

class XmlEmployeesListQueryHandler implements EmployeesListQueryHandlerInterface
{
    public function __construct(
        private XmlManager $xmlManager,
        private Config $config,
    ) {}


    public function handle(EmployeesListQuery $query): EmployeesListQueryResult
    {
        $result = [];

        $employeesCollection = $this->loadEmployeesCollection();
        foreach ($employeesCollection as $employee) {
            array_unshift($result, new Employee(
                $employee->getEmployeeId()->toString(),
                $employee->getName()->toString(),
                $employee->getAge()->toInt(),
                $employee->getSex()->value,
            ));
        }

        return new EmployeesListQueryResult($employeesCollection->count(), $result);
    }



    private function loadEmployeesCollection(): EmployeesCollection
    {
        return $this->xmlManager->loadData($this->config->xmlDocument, EmployeesCollection::class);
    }
}

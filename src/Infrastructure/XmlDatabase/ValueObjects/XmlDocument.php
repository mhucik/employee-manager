<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\ValueObjects;

class XmlDocument
{
    public function __construct(
        private string $path,
    ) {}


    /**
     * @throws \Mhucik\Infrastructure\XmlDatabase\Exceptions\FileContentsLoadFailedException
     */
    public function getContents(): string
    {
        $contents = file_get_contents($this->path);

        if ($contents === false) {
            throw new \Mhucik\Infrastructure\XmlDatabase\Exceptions\FileContentsLoadFailedException();
        }

        return $contents;
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Serializers;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;

class EmployeeIdParser
{
    public static function parse(string|EmployeeId $id): string|EmployeeId
    {
        if ($id instanceof \Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId) {
            return $id->toString();
        }

        return EmployeeId::fromString($id);
    }
}

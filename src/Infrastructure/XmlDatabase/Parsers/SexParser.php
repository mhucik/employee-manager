<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Serializers;

use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use UnitEnum;

class SexParser
{
    public static function parse(string|Sex $sex): string|Sex
    {
        if ($sex instanceof Sex) {
            return $sex->value;
        }

        return Sex::from($sex);
    }
}

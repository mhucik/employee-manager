<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Serializers;

use Mhucik\EmployeeManager\Domain\ValueObjects\Age;

class AgeParser
{
    public static function parse(int|string|Age $age): string|Age
    {
        if ($age instanceof Age) {
            return (string) $age->toInt();
        }

        return Age::fromValue($age);
    }
}

<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Serializers;

use Mhucik\EmployeeManager\Domain\ValueObjects\Name;

class NameParser
{
    public static function parse(string|Name $name): string|Name
    {
        if ($name instanceof \Mhucik\EmployeeManager\Domain\ValueObjects\Name) {
            return $name->toString();
        }

        return Name::fromString($name);
    }
}

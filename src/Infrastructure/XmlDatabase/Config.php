<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase;

use Mhucik\Infrastructure\XmlDatabase\ValueObjects\XmlDocument;

readonly class Config
{
    public XmlDocument $xmlDocument;

    public function __construct(
        public string $pathToXml,
    )
    {
        $this->xmlDocument = new XmlDocument($pathToXml);
    }
}

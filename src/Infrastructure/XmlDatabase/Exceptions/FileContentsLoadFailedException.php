<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Exceptions;

use Exception;

class FileContentsLoadFailedException extends Exception
{
}

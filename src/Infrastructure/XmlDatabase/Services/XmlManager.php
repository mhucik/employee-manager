<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Services;

use Mhucik\Infrastructure\XmlDatabase\ValueObjects\XmlDocument;

class XmlManager
{
    public function __construct(
        private Serializer $serializer,
        private Deserializer $deserializer,
    ) {}


    /**
     * @template T of object
     * @param class-string<T> $class
     * @return T
     */
    public function loadData(XmlDocument $xmlDocument, string $class): object
    {
        return $this->deserializer->deserialize($xmlDocument, $class);
    }


    public function saveData(string $pathToXml, object $object): void
    {
        $xml = $this->serializer->serialize($object);

        $xml->save($pathToXml);
    }
}

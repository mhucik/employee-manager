<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Services;

class Serializer
{
    public function __construct(
        private \KudrMichal\Serializer\Xml\Serializer $serializer,
    ) {}


    public function serialize(object $object): \DOMDocument
    {
        return $this->serializer->serialize($object);
    }
}

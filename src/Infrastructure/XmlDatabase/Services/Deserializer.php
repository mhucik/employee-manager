<?php declare(strict_types = 1);

namespace Mhucik\Infrastructure\XmlDatabase\Services;

use Mhucik\Infrastructure\XmlDatabase\Exceptions\FileContentsLoadFailedException;
use Mhucik\Infrastructure\XmlDatabase\ValueObjects\XmlDocument;

class Deserializer
{
    public function __construct(
        private \KudrMichal\Serializer\Xml\Deserializer $deserializer,
    ) {}


    /**
     * @template T of object
     * @param class-string<T> $class
     * @return T
     */
    public function deserialize(XmlDocument $xmlDocument, string $class): object
    {
        $doc = new \DOMDocument();

        try {
            $doc->loadXML($xmlDocument->getContents());
        } catch (FileContentsLoadFailedException $e) {
            throw new \RuntimeException('Failed to load XML document', previous: $e);
        }

        $data = $this->deserializer->deserialize($doc, $class);

        if ($data instanceof $class) {
            return $data;
        }

        throw new \RuntimeException('Failed to deserialize XML document');
    }
}

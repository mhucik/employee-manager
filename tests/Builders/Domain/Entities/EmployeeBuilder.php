<?php declare(strict_types = 1);

namespace Tests\Builders\Domain\Entities;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Entities\Employee;
use Mhucik\EmployeeManager\Domain\ValueObjects\Age;
use Mhucik\EmployeeManager\Domain\ValueObjects\Name;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use function Tests\Builders\anAge;
use function Tests\Builders\aName;

class EmployeeBuilder
{
    private ?EmployeeId $employeeId = null;
    private ?Name $name = null;
    private ?Age $age = null;
    private ?Sex $sex = null;
    private bool $deleted = false;

    public function build(): Employee
    {
        $employee = new Employee(
            $this->employeeId ?: EmployeeId::generate(),
            $this->name ?: aName()->build(),
            $this->age ?: anAge()->build(),
            $this->sex ?: Sex::MALE,
        );

        if ($this->deleted) {
            $employee->delete();
        }

        return $employee;
    }


    public function withId(EmployeeId $employeeId): self
    {
        $c = clone $this;
        $c->employeeId = $employeeId;

        return $c;
    }


    public function withName(Name $name): self
    {
        $c = clone $this;
        $c->name = $name;

        return $c;
    }


    public function withAge(Age $age): self
    {
        $c = clone $this;
        $c->age = $age;

        return $c;
    }


    public function withSex(Sex $sex): self
    {
        $c = clone $this;
        $c->sex = $sex;

        return $c;
    }


    public function withDeleted(): self
    {
        $c = clone $this;
        $c->deleted = true;

        return $c;
    }
}

<?php declare(strict_types = 1);

namespace Tests\Builders\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\ValueObjects\Name;

class NameBuilder
{
    private ?string $name = null;

    public function build(): Name
    {
        return Name::fromString($this->name ?: 'Tadej Pogačar');
    }


    public function withName(string $name): self
    {
        $c = clone $this;
        $c->name = $name;

        return $c;
    }
}

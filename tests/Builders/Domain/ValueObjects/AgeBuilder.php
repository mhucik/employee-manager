<?php declare(strict_types = 1);

namespace Tests\Builders\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\ValueObjects\Age;

class AgeBuilder
{
    private ?int $age = null;

    public function build(): Age
    {
        return Age::fromValue($this->age ?: 26);
    }


    public function withAge(int $age): self
    {
        $c = clone $this;
        $c->age = $age;

        return $c;
    }
}

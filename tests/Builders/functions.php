<?php declare(strict_types = 1);

namespace Tests\Builders;

use Tests\Builders\Domain\Entities\EmployeeBuilder;
use Tests\Builders\Domain\ValueObjects\AgeBuilder;
use Tests\Builders\Domain\ValueObjects\NameBuilder;

function anEmployee(): EmployeeBuilder
{
    return new EmployeeBuilder();
}

function aName(): NameBuilder
{
    return new NameBuilder();
}

function anAge(): AgeBuilder
{
    return new AgeBuilder();
}

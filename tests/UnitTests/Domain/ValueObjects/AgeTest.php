<?php declare(strict_types = 1);

namespace Tests\UnitTests\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Mhucik\EmployeeManager\Domain\ValueObjects\Age;
use Tests\UnitTests\UnitTestCase;

class AgeTest extends UnitTestCase
{
    public function testAgeCreated(): void
    {
        $age = Age::fromValue(28);

        $this->assertEquals(28, $age->toInt());
    }


    public function testAgeCreatedWithNegativeValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('Age must be greater or equal to %d', Age::AGE_LIMIT));

        Age::fromValue(-1);
    }


    public function testAgeCreatedWithFloatValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Age must be integer');

        Age::fromValue(1.5);
    }


    public function testAgeCreatedWithValidStringValue(): void
    {
        $age = Age::fromValue('18');

        $this->assertEquals(18, $age->toInt());
    }


    public function testAgeCreatedWithInvalidStringValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Age must be integer');

        Age::fromValue('abc');
    }
}

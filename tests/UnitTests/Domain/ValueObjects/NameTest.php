<?php declare(strict_types = 1);

namespace Tests\UnitTests\Domain\ValueObjects;

use Mhucik\EmployeeManager\Domain\Exceptions\InvalidArgumentException;
use Mhucik\EmployeeManager\Domain\ValueObjects\Name;
use Tests\UnitTests\UnitTestCase;

class NameTest extends UnitTestCase
{
    public function testNameCreated(): void
    {
        $name = Name::fromString('Tadej Pogačar');

        $this->assertEquals('Tadej Pogačar', $name->toString());
    }

    public function testNameCreatedWithEmptyValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Name cannot be empty');

        Name::fromString('');
    }


    public function testNameCreatedWithNumericValue(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Name contains invalid characters');

        Name::fromString('123');
    }
}

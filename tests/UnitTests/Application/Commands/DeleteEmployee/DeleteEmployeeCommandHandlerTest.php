<?php declare(strict_types = 1);

namespace Tests\UnitTests\Application\Commands\DeleteEmployee;

use Mhucik\EmployeeManager\Application\Commands\DeleteEmployee\DeleteEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\DeleteEmployee\DeleteEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mockery;
use Tests\UnitTests\UnitTestCase;
use function Tests\Builders\anEmployee;

class DeleteEmployeeCommandHandlerTest extends UnitTestCase
{
    public function testEmployeeDeleted(): void
    {
        $employeeId = EmployeeId::generate();
        $repository = Mockery::mock(EmployeeRepositoryInterface::class)
            ->allows('getById')->andReturn(anEmployee()->withId($employeeId)->build())->once()->getMock()
            ->allows('save')->once()->withArgs($this->captureArg($employee))->getMock()
        ;

        $handler = new DeleteEmployeeCommandHandler($repository);
        $handler->handle(new DeleteEmployeeCommand($employeeId->toString()));

        /** @var \Mhucik\EmployeeManager\Domain\Entities\Employee $employee */
        $this->assertTrue($employee->isDeleted());
    }


    public function testEmployeeNotFound(): void
    {
        $employeeId = EmployeeId::generate();

        $repository = Mockery::mock(EmployeeRepositoryInterface::class)
            ->allows('getById')->once()->andThrow(EmployeeNotFoundException::class, sprintf('Employee with id %s not found.', $employeeId->toString()))->getMock()
        ;
        $handler = new DeleteEmployeeCommandHandler($repository);

        $this->expectExceptionObject(EmployeeNotFoundException::withId($employeeId));
        $handler->handle(new DeleteEmployeeCommand($employeeId->toString()));
    }
}

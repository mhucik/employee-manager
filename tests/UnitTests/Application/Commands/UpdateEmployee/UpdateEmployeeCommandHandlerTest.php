<?php declare(strict_types = 1);

namespace Tests\UnitTests\Application\Commands\UpdateEmployee;

use Mhucik\EmployeeManager\Application\Commands\UpdateEmployee\UpdateEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\UpdateEmployee\UpdateEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use Mockery;
use Tests\UnitTests\UnitTestCase;
use function Tests\Builders\aName;
use function Tests\Builders\anEmployee;

class UpdateEmployeeCommandHandlerTest extends UnitTestCase
{
    public function testEmployeeUpdated(): void
    {
        $employeeId = EmployeeId::generate();
        $employee = anEmployee()
            ->withId($employeeId)
            ->withName(aName()->withName('Tadej Pogačar')->build())
            ->build()
        ;

        $handler = new UpdateEmployeeCommandHandler(
            Mockery::mock(EmployeeRepositoryInterface::class)
                ->allows('getById')->andReturn($employee)->once()->getMock()
                ->allows('save')->withArgs($this->captureArg($savedEmployee))->once()->getMock()
        );

        $handler->handle(new UpdateEmployeeCommand(
            $employeeId->toString(),
            'Jonas Vingegaard',
            26,
            null,
        ));

        /** @var \Mhucik\EmployeeManager\Domain\Entities\Employee $savedEmployee */
        $this->assertEquals('Jonas Vingegaard', $savedEmployee->getName()->toString());
        $this->assertEquals(26, $savedEmployee->getAge()->toInt());
        $this->assertEquals(Sex::MALE->value, $savedEmployee->getSex()->value);
    }


    public function testEmployeeNotFound(): void
    {
        $employeeId = EmployeeId::generate();

        $repository = Mockery::mock(EmployeeRepositoryInterface::class)
            ->allows('getById')
            ->once()
            ->andThrow(EmployeeNotFoundException::class, sprintf('Employee with id %s not found.', $employeeId->toString()))
            ->getMock()
        ;
        $handler = new UpdateEmployeeCommandHandler($repository);

        $this->expectExceptionObject(EmployeeNotFoundException::withId($employeeId));
        $handler->handle(new UpdateEmployeeCommand($employeeId->toString(), null, null, null));
    }
}

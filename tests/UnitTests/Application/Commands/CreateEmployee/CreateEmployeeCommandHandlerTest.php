<?php declare(strict_types = 1);

namespace Tests\UnitTests\Application\Commands\CreateEmployee;

use Mhucik\EmployeeManager\Application\Commands\CreateEmployee\CreateEmployeeCommand;
use Mhucik\EmployeeManager\Application\Commands\CreateEmployee\CreateEmployeeCommandHandler;
use Mhucik\EmployeeManager\Domain\Repositories\EmployeeRepositoryInterface;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use Mockery;
use Tests\UnitTests\UnitTestCase;

class CreateEmployeeCommandHandlerTest extends UnitTestCase
{
    public function testEmployeeCreated(): void
    {
        $createEmployeeCommandHandler = new CreateEmployeeCommandHandler(
            Mockery::mock(EmployeeRepositoryInterface::class)
                ->allows('save')
                ->once()
                ->withArgs($this->captureArg($employee))
                ->getMock()
        );

        $createEmployeeCommandHandler->handle(new CreateEmployeeCommand(
            'Wout van Aert',
            28,
            Sex::MALE->value,
        ));

        /** @var \Mhucik\EmployeeManager\Domain\Entities\Employee $employee */
        $this->assertEquals('Wout van Aert', $employee->getName()->toString());
        $this->assertEquals(28, $employee->getAge()->toInt());
        $this->assertEquals(Sex::MALE->value, $employee->getSex()->value);
    }
}

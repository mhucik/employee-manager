<?php declare(strict_types = 1);

namespace Tests\UnitTests;

use PHPUnit\Framework\TestCase;

abstract class UnitTestCase extends TestCase
{
    protected function captureArg(mixed &$arg): callable
    {
        return function(mixed $argToMock) use (&$arg): bool {
            $arg = $argToMock;
            return true;
        };
    }


    public function tearDown(): void
    {
        \Mockery::close();

        parent::tearDown();
    }
}

<?php declare(strict_types = 1);

namespace Tests\IntegrationTests;

use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;
use Nette\DI\Container;
use PHPUnit\Framework\TestCase;
use Tests\IntegrationTests\Fixtures\FixtureInterface;

abstract class IntegrationTestCase extends TestCase
{
    protected const TEST_EMPLOYEES_COLLECTION_XML = __DIR__ . '/testEmployeesCollectionDatabase.xml';
    private static ?Container $container = null;
    /**
     * @var array<string, \Tests\IntegrationTests\Fixtures\FixtureInterface>
     */
    private array $fixtures = [];


    protected function setUp(): void
    {
        parent::setUp();

        foreach ($this->fixtures as $fixture) {
            $fixture->load($this->getContainer()->getByType(XmlManager::class));
        }
    }


    protected function tearDown(): void
    {
        parent::tearDown();
        $this->cleanUp();
    }


    protected function addFixture(FixtureInterface $fixture): void
    {
        $this->fixtures[] = $fixture;
    }


    protected function getContainer(): Container
    {
        if (self::$container === null) {
            self::$container = $this->createContainer();
        }

        return self::$container;
    }


    private function cleanUp(): void
    {
        file_put_contents(self::TEST_EMPLOYEES_COLLECTION_XML, "<?xml version=\"1.0\"?>\n<listEmployees></listEmployees>");
    }


    private function createContainer(): Container
    {
        require __DIR__ . '/../../src/bootstrap.php';

        /** @var \Nette\Bootstrap\Configurator $configurator */
        $configurator->addConfig(__DIR__ . '/config.neon');

        return $configurator->createContainer();
    }
}

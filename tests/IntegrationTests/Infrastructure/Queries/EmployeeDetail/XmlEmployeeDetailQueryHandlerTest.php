<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Infrastructure\EmployeeDetail;

use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQuery;
use Mhucik\EmployeeManager\Application\Queries\EmployeeDetail\EmployeeDetailQueryResult;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use Ramsey\Uuid\Uuid;
use Tests\IntegrationTests\Fixtures\EmployeeFixture;
use Tests\IntegrationTests\IntegrationTestCase;

class XmlEmployeeDetailQueryHandlerTest extends IntegrationTestCase
{
    protected function setUp(): void
    {
        $this->addFixture(new EmployeeFixture(self::TEST_EMPLOYEES_COLLECTION_XML));

        parent::setUp();
    }


    public function test_handler_dataReturned(): void
    {
        $employeeDetailQueryHandler = $this->getContainer()->getByName('xmlEmployeeDetailQueryHandler');

        $result = $employeeDetailQueryHandler->handle(new EmployeeDetailQuery(EmployeeFixture::TOM_PIDCOCK_ID));

        $this->assertInstanceOf(EmployeeDetailQueryResult::class, $result);
        $this->assertSame(EmployeeFixture::TOM_PIDCOCK_ID, $result->id);
        $this->assertSame('Tom Pidcock', $result->name);
        $this->assertSame(23, $result->age);
        $this->assertSame(Sex::MALE->value, $result->sex);
    }


    public function test_handler_employeeNotFound_exceptionThrown(): void
    {
        $employeeDetailQueryHandler = $this->getContainer()->getByName('xmlEmployeeDetailQueryHandler');

        $this->expectException(\Mhucik\EmployeeManager\Domain\Exceptions\EmployeeNotFoundException::class);

        $employeeDetailQueryHandler->handle(new EmployeeDetailQuery(Uuid::NIL));
    }
}

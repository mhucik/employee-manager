<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Infrastructure\AgeStatsData;

use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQuery;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\AgeStatsDataQueryResult;
use Mhucik\EmployeeManager\Application\Queries\AgeStatsData\GroupedAgeStatsDataResult;
use Tests\IntegrationTests\Fixtures\EmployeeFixture;
use Tests\IntegrationTests\IntegrationTestCase;

class XmlAgeStatsDataQueryHandlerTest extends IntegrationTestCase
{
    protected function setUp(): void
    {
        $this->addFixture(new EmployeeFixture(self::TEST_EMPLOYEES_COLLECTION_XML));

        parent::setUp();
    }


    public function testDataReturned(): void
    {
        $binSize = 5;
        $xmlAgeStatsDataQueryHandler = $this->getContainer()->getByName('xmlAgeStatsDataQueryHandler');

        $result = $xmlAgeStatsDataQueryHandler->handle(new AgeStatsDataQuery($binSize));

        $this->assertCount($binSize, $result->maleStats);
        $this->assertCount($binSize, $result->femaleStats);
        $this->assertCount($binSize, $result->totalStats);
        $this->assertInstanceOf(GroupedAgeStatsDataResult::class, $result->maleStats[0]);
        $this->assertSame(10, array_sum(array_map(fn(GroupedAgeStatsDataResult $ageStatsDataQueryResult) => $ageStatsDataQueryResult->frequency, $result->totalStats)));
    }
}

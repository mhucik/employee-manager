<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Infrastructure\Queries;

use Mhucik\EmployeeManager\Application\Queries\EmployeesList\EmployeesListQuery;
use Tests\IntegrationTests\Fixtures\EmployeeFixture;
use Tests\IntegrationTests\IntegrationTestCase;

class XmlEmployeesListQueryHandlerTest extends IntegrationTestCase
{
    protected function setUp(): void
    {
        $this->addFixture(new EmployeeFixture(self::TEST_EMPLOYEES_COLLECTION_XML));

        parent::setUp();
    }


    public function testDataReturned(): void
    {
        /** @var \Mhucik\Infrastructure\Queries\EmployeesList\XmlEmployeesListQueryHandler $xmlEmployeesListQueryHandler */
        $xmlEmployeesListQueryHandler = $this->getContainer()->getByName('xmlEmployeesListQueryHandler');

        $result = $xmlEmployeesListQueryHandler->handle(new EmployeesListQuery());

        $this->assertSame(10, $result->total);
    }
}

<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Infrastructure\Repositories;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Tests\IntegrationTests\Fixtures\EmployeeFixture;
use Tests\IntegrationTests\IntegrationTestCase;
use function Tests\Builders\anAge;
use function Tests\Builders\aName;
use function Tests\Builders\anEmployee;

class XmlEmployeeRepositoryTest extends IntegrationTestCase
{
    protected function setUp(): void
    {
        $this->addFixture(new EmployeeFixture(self::TEST_EMPLOYEES_COLLECTION_XML));

        parent::setUp();
    }


    public function test_save(): void
    {
        $xmlEmployeeRepository = $this->getContainer()->getByName('xmlEmployeeRepository');

        $xmlEmployeeRepository->save(
            anEmployee()
                ->withName(aName()->withName('Magnus Cort Nielsen')->build())
                ->withAge(anAge()->withAge(30)->build())
                ->build()
        );

        $fileContents = file_get_contents(self::TEST_EMPLOYEES_COLLECTION_XML);

        $this->assertStringContainsString('<name>Magnus Cort Nielsen</name>', $fileContents);
        $this->assertStringContainsString('<age>30</age>', $fileContents);
    }


    public function test_updateEmployee(): void
    {
        $xmlEmployeeRepository = $this->getContainer()->getByName('xmlEmployeeRepository');

        $employee = $xmlEmployeeRepository->getById(EmployeeId::fromString(EmployeeFixture::TOM_PIDCOCK_ID));

        $employee->update(
            anAge()->withAge(99)->build(),
            aName()->withName('Tom Pidcock')->build(),
            null
        );

        $xmlEmployeeRepository->save($employee);

        $fileContents = file_get_contents(self::TEST_EMPLOYEES_COLLECTION_XML);

        $this->assertEquals(1, substr_count($fileContents, '<name>Tom Pidcock</name>'));
        $this->assertEquals(1, substr_count($fileContents, '<age>99</age>'));
    }


    public function test_save_deleteEmployee(): void
    {
        /** @var \Mhucik\Infrastructure\Repositories\XmlEmployeeRepository $xmlEmployeeRepository */
        $xmlEmployeeRepository = $this->getContainer()->getByName('xmlEmployeeRepository');

        $employee = $xmlEmployeeRepository->getById(EmployeeId::fromString(EmployeeFixture::TOM_PIDCOCK_ID));

        $employee->delete();

        $xmlEmployeeRepository->save($employee);

        $fileContents = file_get_contents(self::TEST_EMPLOYEES_COLLECTION_XML);

        $this->assertStringNotContainsString(sprintf('<id>%s</id>', EmployeeFixture::TOM_PIDCOCK_ID), $fileContents);
    }


    public function test_getById(): void
    {
        $xmlEmployeeRepository = $this->getContainer()->getByName('xmlEmployeeRepository');

        $employee = $xmlEmployeeRepository->getById(EmployeeId::fromString(EmployeeFixture::TOM_PIDCOCK_ID));

        $this->assertEquals('Tom Pidcock', $employee->getName()->toString());
        $this->assertEquals(23, $employee->getAge()->toInt());
    }
}

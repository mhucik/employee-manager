<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Fixtures;

use Mhucik\EmployeeManager\Domain\AggregateId\EmployeeId;
use Mhucik\EmployeeManager\Domain\Entities\EmployeesCollection;
use Mhucik\EmployeeManager\Domain\ValueObjects\Sex;
use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;
use function Tests\Builders\anAge;
use function Tests\Builders\aName;
use function Tests\Builders\anEmployee;

class EmployeeFixture implements FixtureInterface
{
    public const TOM_PIDCOCK_ID = 'd8106a16-2bf2-4478-a26f-293118fbfb90';


    public function __construct(
        private string $databasePath,
    ) {}


    public function load(XmlManager $xmlManager): void
    {
        $employeesCollection = new EmployeesCollection(
            anEmployee()
                ->withName(aName()->withName('Yves Lampaert')->build())
                ->withAge(anAge()->withAge(31)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Fabio Jakobsen')->build())
                ->withAge(anAge()->withAge(26)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Dylan Groenewegen')->build())
                ->withAge(anAge()->withAge(26)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Jonas Vingegaard')->build())
                ->withAge(anAge()->withAge(26)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withId(EmployeeId::fromString(self::TOM_PIDCOCK_ID))
                ->withName(aName()->withName('Tom Pidcock')->build())
                ->withAge(anAge()->withAge(23)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Wout van Aert')->build())
                ->withAge(anAge()->withAge(28)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Tadej Pogačar')->build())
                ->withAge(anAge()->withAge(24)->build())
                ->withSex(Sex::MALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Annemiek van Vleuten')->build())
                ->withAge(anAge()->withAge(40)->build())
                ->withSex(Sex::FEMALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Demi Vollering')->build())
                ->withAge(anAge()->withAge(26)->build())
                ->withSex(Sex::FEMALE)
                ->build(),
            anEmployee()
                ->withName(aName()->withName('Silvia Persico')->build())
                ->withAge(anAge()->withAge(25)->build())
                ->withSex(Sex::FEMALE)
                ->build(),
        );

        $xmlManager->saveData($this->databasePath, $employeesCollection);
    }
}

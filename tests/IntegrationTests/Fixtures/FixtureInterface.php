<?php declare(strict_types = 1);

namespace Tests\IntegrationTests\Fixtures;

use Mhucik\Infrastructure\XmlDatabase\Services\XmlManager;

interface FixtureInterface
{
    public function load(XmlManager $xmlManager): void;
}

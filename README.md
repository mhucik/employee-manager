## Setup project using docker

### Setup environment

You can specify port on which the application will be running by editing docker-compose.yml `ports`

### Run docker container

```sh
docker-compose up -d
```

### Install composer dependencies

You can install by your own local composer or use composer inside the docker container.

```sh
docker-compose exec php composer install
```

### Install sample data

```sh
docker-compose exec php bin/console app:initDb
```

And that's it. You can now access the application

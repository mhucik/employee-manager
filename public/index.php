<?php declare(strict_types = 1);

use Contributte\Middlewares\Application\IApplication;

require __DIR__ . '/../src/bootstrap.php';
$container = $configurator->createContainer();
$mezzioApp = $container->getByType(IApplication::class);

$mezzioApp->run();
